import { createSelector } from 'reselect';

const myItems = state => (state) ? state.orders : false;

export default createSelector(
  myItems,
  items => (items) ? items.reduce((acc, cV) => acc + cV.totalPrice, 0) : 0
);
