import { createSelector } from 'reselect';

const selectMoviesList = state => state;

export default createSelector(
  selectMoviesList,
  ({ movies, filter, query }) => {
    let newMoviesList = [];

    if (!filter || filter === 'All') {
      newMoviesList = movies;
    } else {
      newMoviesList = movies.filter(movie => movie.genres.includes(filter))
    }

    if (query) {
      newMoviesList = newMoviesList.filter(movie => movie.title.indexOf(query) === -1 ? 0 : 1)
    }

    return newMoviesList;
  }
);
