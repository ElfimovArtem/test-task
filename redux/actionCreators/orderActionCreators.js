export const ADD_MOVIE_TO_ORDER = 'ADD_MOVIE_TO_ORDER';
export const REMOVE_MOVIE_FROM_ORDER = 'REMOVE_MOVIE_FROM_ORDER';
export const CONFIRM_ORDER = 'CONFIRM_ORDER';
export const DENY_ORDER = 'DENY_ORDER';

export const addMovie = (movie) => {
  return {
    type: ADD_MOVIE_TO_ORDER,
    movie: {
      ...movie
    }
  }
};
export const removeMovie = (movieID) => {
  return {
    type: REMOVE_MOVIE_FROM_ORDER,
    movieId: movieID
  }
};

export const confirmOrder = (order) => {
  return {
    type: CONFIRM_ORDER,
    order: {
      ...order
    }
  }
};
export const denyOrder = (err) => {
  return {
    type: DENY_ORDER,
    error: err
  }
};
export const completeOrder = () => (dispatch, getState) => {
  if (getState().account.user.token) {
    return dispatch(confirmOrder(getState().order));
  } else {
    return dispatch(denyOrder('Error'));
  }
};
