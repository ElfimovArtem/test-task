import { ADD_MOVIE_TO_ORDER, CONFIRM_ORDER, REMOVE_MOVIE_FROM_ORDER } from '../actionCreators/orderActionCreators';
import { LOGOUT } from '../actionCreators/accountActionCreators';

const initialState = { movies: [], totalPrice: 0 };

const currentOrderReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_MOVIE_TO_ORDER:
      return {
        ...state,
        movies:
          [
            ...state.movies,
            { ...action.movie }
          ],
        totalPrice: state.totalPrice + action.movie.price
      };
    case REMOVE_MOVIE_FROM_ORDER:
      const newMoviesArr = state.movies.filter(movie => {
        if (movie.id !== action.movieId) {
          return movie;
        }
      });

      return {
        ...state,
        movies: newMoviesArr,
        totalPrice: newMoviesArr.reduce((acc, cV) => acc + cV.price, 0)
      };
    case CONFIRM_ORDER:
      return initialState;
    case LOGOUT:
      return {
        movies: [],
        totalPrice: 0
      };
    default:
      return state;
  }
};

export default currentOrderReducer;
