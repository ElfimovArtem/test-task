import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Field, reduxForm } from 'redux-form';

import FormInput from '../../components/FormInput';
import Button from '../../components/Button';
import {
  REQUIRED, TOO_LONG, TOO_SHORT, NOT_MATCHING
} from '../../model/ValidationErrors';

const UsernameInput = props => (
  <FormInput label="Username" placeholder="Type your username..." {...props} />
);

const PasswordInput = props => (
  <FormInput isSecure label="Password" placeholder="Type your password..." {...props} />
);

const ConfirmationInput = props => (
  <FormInput isSecure label="Confirmation" placeholder="Confirm your password..." {...props} />
);


const required = value => value ? undefined : REQUIRED;
const maxLength = value => (value && value.length > 15) ? TOO_LONG(15) : undefined;
const minLength = value => (value && value.length < 6) ? TOO_SHORT(6) : undefined;
const verificationCheck = (value, confirm) => (value && value !== confirm.password) ? NOT_MATCHING : undefined;

class SignUpForm extends React.PureComponent {
  render() {
    const { handleSubmit } = this.props;

    return (
      <View style={styles.container}>
        <Field
          component={UsernameInput}
          validate={[ required, maxLength ]}
          name={"username"}
        />
        <Field
          component={PasswordInput}
          validate={[ required, minLength ]}
          name={"password"}
        />
        <Field
          component={ConfirmationInput}
          validate={[ required, verificationCheck]}
          name={"confirmation"}
        />
        <Button title="Sign up!" onPress={handleSubmit} />
      </View>
    );
  }
}

SignUpForm = reduxForm(
  {
    form: 'signUp',
  }
)(SignUpForm);

export default SignUpForm;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  title: {
    textAlign: 'center',
    fontSize: 18,
    marginTop: 20,
    fontWeight: 'bold'
  },
  subtitle: {
    textAlign: 'center',
    fontSize: 14,
    marginTop: 8,
    marginBottom: 20
  },
  noAccount: {
    textAlign: 'center'
  }
});
